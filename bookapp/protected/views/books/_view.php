<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_name')); ?>:</b>
	<?php echo CHtml::encode($data->book_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_image')); ?>:</b>
	<?php echo CHtml::encode($data->book_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('published_year')); ?>:</b>
	<?php echo CHtml::encode($data->published_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_status')); ?>:</b>
	<?php echo CHtml::encode($data->book_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />


</div>