<?php
$this->breadcrumbs=array(
	'Books',
);

$this->menu=array(
	array('label'=>'Create Books','url'=>array('create')),
	array('label'=>'Manage Books','url'=>array('admin')),
);
?>

<h1>Books</h1>

<?php //$this->widget('bootstrap.widgets.TbListView',array(
//	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
//)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'books-grid',
    'dataProvider'=>$dataProvider,
    //'filter'=>$model,
    'columns'=>array(
        'id',
        'book_name',
    //        'book_image',
        'published_year',
        'book_status',
        'user_id',
        array(
            'name'=>'book_image',
            'value'=> 'TbHtml::imageCircle(Yii::app()->baseUrl."/images/uploaded/".$data->book_image)',
            'type'=>'raw',
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        ),
    ),
));

?>
