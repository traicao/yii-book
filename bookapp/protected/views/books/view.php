<?php
$this->breadcrumbs=array(
	'Books'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Books','url'=>array('index')),
	array('label'=>'Create Books','url'=>array('create')),
	array('label'=>'Update Books','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Books','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Books','url'=>array('admin')),
);
?>

<h1>View Books #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'book_name',
		'book_image',
		'published_year',
		'book_status',
		'user_id',
	),
));


?>
