/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.7.19-0ubuntu0.16.04.1 : Database - book
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`book` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `book`;

/*Table structure for table `books` */

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `book_image` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_year` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_status` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `books` */

insert  into `books`(`id`,`book_name`,`book_image`,`published_year`,`book_status`,`user_id`) values 
(1,'Hiiiiiiiii','aaa','1999','public',2),
(2,'Math','dddddd','2000','public',2),
(3,'aaaaaa','aaaaaaaa','2017','protected',1),
(4,'nnnnnnnn',NULL,'1111','protected',1),
(5,'nnnnnnnn',NULL,'2000','protected',1),
(6,'nnnnnnnn',NULL,'1999','protected',1),
(7,'nnnnnnnn','download.jpg','1999','protected',1),
(8,'nnnnnnnn','download.jpg','2000','protected',1),
(9,'nnnnnnnn','download.jpg','2000','protected',1),
(10,'nam','download.jpg','3000','protected',1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pwd_hash` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`pwd_hash`,`email`,`image`,`role`) values 
(1,'crtrai','$1$Gl6DMU1G$FjosIoM2yZpUVVoifLrIy1','trai@123.com',NULL,NULL),
(2,'trai','$1$xf2kEmiK$c/xJmwSGtgVvMML3rjme7/','asd@askjhd.com',NULL,NULL),
(3,'admin','$1$SIZ14FZV$u1d/qMbU3F9stpAaDuvkZ.','trai@123.com',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
